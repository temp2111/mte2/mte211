let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){

}

//Adds an element/elements to the end of the queue
function enqueue(item){

}

//Removes an element in front of the queue
function dequeue(){

}


//Shows the element at the front
function front(){

}

//Shows the total number of elements
function size(){

}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){

}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};